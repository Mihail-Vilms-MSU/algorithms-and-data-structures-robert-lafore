package Chapter_7_Advanced_Sorting;

/**
 * Method shellSort() in ArraySh class implements "Shellsort" algorithm
 */
public class ArraySh {
    private long[] array;
    private int actualLength;

    public ArraySh(int allocLength) {
        array = new long[allocLength];
        actualLength = 0;
    }

    public void insert(long value) {
        array[actualLength] = value;
        actualLength++;
    }

    public void display(){
        System.out.print("Array: {");
        for (int j = 0; j < actualLength; j++)
            System.out.print(array[j] + "; ");
        System.out.print("}");
        System.out.println("");
    }

    public void myShellSort(){
        int h = 1;
        int outerPivot;
        int innerPivot;
        long temp;


        // Knuth expression for sequence of gaps(h) - "h=h*3+1"
        while(h <= actualLength/3) {
            h = h*3 + 1;
        }

        while (h >= 1){
            System.out.println(" ~~~ Current gap - " + h + " ~~~ ");
            outerPivot = h;

            while(outerPivot < actualLength) {
                temp = array[outerPivot];
                innerPivot = outerPivot - h;

                while((innerPivot >= 0) && (array[innerPivot] > temp)){
                    array[innerPivot + h] = array[innerPivot];
                    array[innerPivot] = temp;
                    System.out.println("Shift - " + array[innerPivot + h] + " and " + array[innerPivot]);
                    innerPivot = innerPivot - h;
                    display();
                }

                outerPivot++;
            }

            h = (h - 1)/3;
        }
    }
}
