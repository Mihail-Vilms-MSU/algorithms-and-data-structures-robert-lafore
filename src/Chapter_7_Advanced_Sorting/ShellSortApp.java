package Chapter_7_Advanced_Sorting;

public class ShellSortApp {
    public static void main(String[] args){
        ArraySh arr = new ArraySh(30);
        arr.insert(17);
        arr.insert(20);
        arr.insert(11);
        arr.insert(19);
        arr.insert(12);
        arr.insert(15);
        arr.insert(18);
        arr.insert(16);
        arr.insert(14);
        arr.insert(13);
        arr.insert(17);
        arr.insert(10);
        arr.insert(1);
        arr.insert(9);
        arr.insert(2);
        arr.insert(5);
        arr.insert(8);
        arr.insert(6);
        arr.insert(4);
        arr.insert(3);
        arr.display();
        arr.myShellSort();
        arr.display();

    }
}
